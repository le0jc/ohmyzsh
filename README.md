# ohmyzsh 国内gitee源安装脚本

#### 介绍
本脚本只是将 https://github.com/ohmyzsh/ohmyzsh 中的安装脚本替换成 gitee 镜像源，免得自己去改……就是这么懒。其他完全和官方一样。

#### 安装方法
与官方相同，可用curl, wget, 或者fetch，运行右边命令即可：

| Method    | Command                                                                                           |
|:----------|:--------------------------------------------------------------------------------------------------|
| **curl**  | `sh -c "$(curl -fsSL https://gitee.com/le0jc/ohmyzsh/raw/master/install.sh)"` |
| **wget**  | `sh -c "$(wget -O- https://gitee.com/le0jc/ohmyzsh/raw/master/install.sh)"`   |
| **fetch** | `sh -c "$(fetch -o - https://gitee.com/le0jc/ohmyzsh/raw/master/install.sh)"` |

#### 修改更新源的方法

按上面方法安装完后，以后oh-my-zsh有更新时，会去gitee下载。gitee的mirror是每天更新，一般来说足够。如果你需要更快的更新，可以选择用fastgit.org等代理服务，但稳定性没有gitee好。

修改方式：
```
cd ~/.oh-my-zsh
git remote set-url origin https://hub.fastgit.org/ohmyzsh/ohmyzsh.git
```
